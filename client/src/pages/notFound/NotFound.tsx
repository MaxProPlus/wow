import React from 'react'
import Page from '../../components/page/Page'

const NotFound: React.FC = () => {
    return (
        <Page>Страница не найдена или еще не создана или нет в тз ¯\_(ツ)_/¯</Page>
    )
}

export default NotFound